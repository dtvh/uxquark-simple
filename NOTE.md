İlgili folderda GIT Subversion'u kaldırmak için >>>>>> find . -name ".git*" -exec rm -R {} \;

Proje başlangıcında "sudo npm install" & "bower install" yapmayı unutmayalım :)

Version.md --->> Dosyanın yapısı aşağıda ki gibi olması gerekiyor. NodeJS her bir satır için farklı işlemler yapmakta. Herhangi bir değişiklik JS tarafını komple patlatacaktır :)

 /* --------- Versiyon : 0.1.0 ------------------- */
 ## Versiyon 0.1.0
 ## IssueId      : DBLU-1326
 ## Fix          : Initial Setup | Grunt tasks | Bower components | Node modules
 /* ---------------------------------------------- */

 Version    : > UXQuark'ın paket dahilinde olan version numarasıdır. Buraya yapılan güncellemeye göre minor veya major numaraların attırılması sağlanarak
            ilgili paketin yeni version numarası yazılmalıdır
 IssueId    : > Bu versionun JIRA üzerinde hangi issue denk geldiğini belirmek için JIRA üzerinde Issue numarası yazılmalı
 Fix        : > Paket içerisine dahil edilmiş güncelleştirmeler listesini " | " şeklinde ayırarak tek satır halinde eklenmesi gerekiyor.
            Burada yazılacak olan metinler JIRA üzerinde ilgili Issue Resolve veya Comment edildiğinde yorum alanınıza extradan dahil edilecek alandır.

<< COMMAND >>

grunt watch > Version.md + JS ve CSS dosyaları üzerinde anlık değişimleri compile edecektir. Bu işlem açıkken console üzerinde başka bir komuta izin vermeyeceği için diğer komut işlemleriniz için Watch'ı durdurmanız gerekiyor.
grunt build > Tek seferlik tüm JS ve CSS kütüphanesini compile edecek. Version.md üzerindeki Versiyon numarası ile yeni bir paket oluşturuacaktır.
node start  > Projeyi JIRA veya Bitbucket'a bağlanarak hem JIRA taskları üzerindeki işlemleri hemde Git üzerinde commit
            işlemlerini sorulan sorular doğrultusunda işlemlerini gerçekleştirecektir.