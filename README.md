# UXQuark Simple Project Dökümantasyonu
  

> Tüm işlemlere başlamadan önce **NodeJS**, **NPM** ve **Bower** kurulu olması gerekiyor.
> Tüm kurulumları gerçekleştirdikten sonra, start verilecek projenin folder'ını oluşturmanız gerekmektedir. Daha sonra Bitbucket bağlantısını yaptıktan sonra oluşturduğunuz proje dosya içerisine isterseniz direk **zip** olarak isteseniz **git clone** ile **https://bitbucket.org/dtvh/uxquark-install** adresinden dosyaları çekebilirsiniz.

## UXQuark Simple Information
UXQuark yapısın da versionlamalarımız **Version.md** klasörü üzerinden yapılmakta. Bu sebeple version.md klasörü üzerinde satır olarak herhangi bir değişiklik yapmayınız.

**Örnek version.md**

```
/* --------- Versiyon : 0.1.0 ------------------- */
## Versiyon 0.1.0
## IssueId      : DBLU-1326
## Fix          : Initial Setup | Grunt tasks | Bower components | Node modules
/* ---------------------------------------------- */
```

**Version** > UXQuark'ın paket dahilinde olan version numarasıdır. Buraya yapılan güncellemeye göre minor veya major numaraların attırılması sağlanarak ilgili paketin yeni version numarası yazılmalıdır.

**IssueId** > Bu versionun JIRA üzerinde hangi issue denk geldiğini belirmek için JIRA üzerinde Issue numarası yazılmalı

**Fix** > Paket içerisine dahil edilmiş güncelleştirmeler listesini " | " şeklinde ayırarak tek satır halinde eklenmesi gerekiyor. Burada yazılacak olan metinler JIRA üzerinde ilgili Issue Resolve veya Comment edildiğinde yorum alanınıza extradan dahil edilecek alandır.

### UXQuark Simple Komutları 

```sh
grunt watch

```


**Açıklama** > Version.md + JS ve CSS dosyaları üzerinde anlık değişimleri compile edecektir. Bu işlem açıkken console üzerinde başka bir komuta izin vermeyeceği için diğer komut işlemleriniz için Watch'ı durdurmanız gerekiyor.


```sh
grunt build
```


**Açıklama** > Tek seferlik tüm JS ve CSS kütüphanesini compile edecek. Version.md üzerindeki Versiyon numarası ile yeni bir paket oluşturulacaktır.


```sh
node start
```


**Açıklama** > Projeyi JIRA veya Bitbucket'a bağlanarak hem JIRA taskları üzerindeki işlemleri hemde Git üzerinde commit işlemlerini sorulan sorular doğrultusunda işlemlerini gerçekleştirecektir.