/**
 * @author Doğan TV Software Section
 */

'use strict';

var gulp = require('gulp');

module.exports = function(){
    var tasks = {
        'reload-sass': {
            tasks: ['sass']
        },
        'reload-icons': {
            tasks: ['icons', 'sass']
        }
    };

    return tasks;
};