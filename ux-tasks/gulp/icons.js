/**
 * @author Doğan TV Software Section
 */

var gulp = require('gulp'),
    config = require('../config'),
    iconfont = require('gulp-iconfont'),
    consolidate = require('gulp-consolidate'),
    rename = require('gulp-rename'),
    notify = require('gulp-notify'),
    runTimestamp = Math.round(Date.now() / 1000);

module.exports = function() {
    return gulp.src(config.paths.svg + '**/*.svg')
        .pipe(iconfont({
            fontName     : config.project.name,
            appendUnicode: true,
            formats      : ['ttf', 'eot', 'woff'],
            timestamp    : runTimestamp
        })).on('glyphs', function(glyphs) {
            gulp.src(config.paths.icontheme + 'icons.tpl')
                .pipe(consolidate('lodash', {
                    glyphs: glyphs,
                    prefix: 'icon-'
                }))
                .pipe(rename('_icons.scss'))
                .pipe(notify('Icon styles created'))
                .pipe(gulp.dest(config.paths.scss + 'gui/elements/'));
        })
        .pipe(gulp.dest(config.paths.fonts));
};