/**
 * @author Doğan TV Software Section
 */

'use strict';

var gulp = require('gulp');

module.exports = function() {
    return gulp.src('**\/*.shtml');
};