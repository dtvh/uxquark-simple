/**
 * @author Doğan TV Software Section
 */

'use strict';

var config = {};

config.project = {
    name : 'blu'
};

config.paths = {
    assets:    'assets/',
    fonts:     'assets/fonts/',
    images:    'assets/images/',
    scripts:   'assets/js/',
    styles:    'assets/css/',
    scss:      'ux-quark/scss/',
    app:       'ux-quark/js/',
    svg:       'ux-quark/svg/',
    icontheme: 'ux-quark/templates/icon/'
};

config.namespace = {
    templates: 'uxquark.templates'
};

config.banner = [
    '/*! DoganTV UI \n' +
    ' *  <%= package.name %> - <%= package.appname %> \n' +
    ' *  <%= package.description %> \n' +
    ' *  @author <%= package.author %> \n' +
    '<% package.contributors.forEach(function(contributor) { %>' +
    ' *          <%= contributor.name %> <<%=contributor.email %>> (<%=contributor.url %>)\n' +
    '<% }) %>' +
    ' *  @build ' + new Date() + '\n' +
    ' */\n'
].join('');

config.vendors = [
    config.paths.app + 'vendors/*.js'
];

config.vendorsBanner = [
    '/*! DoganTV UI Lib \n',
    ' *  ',
    config.vendors.join('\n    ').replace(/ux-quark\/ext\//g, ''),
    ' \n',
    ' *  @build ' + new Date() + '\n',
    ' */\n'
].join('');

config.scripts = [
    config.paths.app + '**/*.js'
];

module.exports = config;
