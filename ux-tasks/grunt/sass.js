/**
 * @author Doğan TV Software Section
 */

module.exports = {
    dist: {
        options: {
            style: 'compressed'
        },
        files  : {
            '<%= paths.styles %>uxquark.min.css': '<%= paths.scss %>styles.scss'
        }
    }
};