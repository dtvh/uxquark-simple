/**
 * @author Doğan TV Software Section
 */

module.exports = {
    version  : {
        files: '<%= paths.assets %>version.md',
        tasks: ['release']
    },
    css      : {
        files: ['**/*.scss'],
        tasks: ['sass']
    },
    js       : {
        files: ['<%= jshint.files %>'],
        tasks: ['jshint', 'concat:app', 'uglify:app']
    }
};