/**
 * @author Doğan TV Software Section
 */

module.exports = {
    options: {
        max_jshint_notifications: 5,
        title                   : "<%= package.fullname %>",
        success                 : true,
        duration                : 5
    }
};