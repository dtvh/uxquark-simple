/**
 * @author Doğan TV Software Section
 */

var QuarkJS = QuarkJS || {};

(function(app) {
    'use strict';

    app.initialized = false;

    app.init = function() {
        if(app.initialized) {
            console.warn('%cUXQuark', 'font-weight: bold', 'is allready initialized');
            return;
        }

        setTimeout(function() {
            app.Version.init();
        });

        app.initialized = true;
    };
})(QuarkJS);

$(QuarkJS.init);