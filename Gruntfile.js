/**
 * @author Doğan TV Software Section
 */
 
// Import common tasks.
var versioner   = require('./ux-tasks/common/versioner');

module.exports = function(grunt) {
    var path = require('path'),
        config = require('./ux-tasks/config');

    grunt.registerTask('release', function(){
        versioner(config);
    });

    require('load-grunt-config')(grunt, {
        configPath: path.join(process.cwd(), './ux-tasks/grunt'),
        data: config
    });
};