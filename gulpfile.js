/**
 * @author Doğan TV Software Section
 */

'use strict';

// Gulp and plugins
var gulp = require('gulp'),
    config = require('./ux-tasks/config'),

// task configs
    icons = require('./ux-tasks/gulp/icons'),
    sass = require('./ux-tasks/gulp/sass'),
    versioner = require('./ux-tasks/common/versioner');


gulp.task('reload-sass', ['sass'], function() {});
gulp.task('reload-icons', ['icons', 'sass'], function() {});

gulp.task('icons', icons);

gulp.task('release', function() {
    versioner(config);
});

gulp.task('build', ['release', 'sass']);
gulp.task('watch', ['build'], function() {

    // --------------------------
    // watch:version
    // --------------------------
    gulp.watch(config.paths.assets + 'version.md', ['release', 'reload-sass']);

    // --------------------------
    // watch:sass
    // --------------------------
    gulp.watch(config.paths.scss + '**/*.scss', ['reload-sass']);

    // --------------------------
    // watch:images
    // --------------------------
    gulp.watch(config.paths.svg + '**/*.svg', ['reload-icons']);
});

gulp.task('default', ['watch']);