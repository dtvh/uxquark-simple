// Get dependencies.
var _ = require('underscore');
var fs = require('fs');
var chalk = require('chalk');

var inquirer = require('inquirer');
var git = require('simple-git')('./');

JiraApi = require('jira').JiraApi;

// Path Files.
var versionMDPath  = 'assets/version.md',
    versionMD = fs.readFileSync(versionMDPath, "utf8");

// Tanımlamalar.
var pkg = versionMD.split('\n')[1].split(' ')[2].replace(/(\r\n|\n|\r)/gm, ""),
    issueID = versionMD.split('\n')[2].split(': ')[1],
    vFix = versionMD.split('\n')[3].split(': ')[1];

git.init().then(function() {}).branch(function(err, data){
    var branchList = _.map(_.pluck(data.branches, 'name'), function(val){ return {name: val}; });

    var Questions = [
        {
            type: "list",
            name: "proccess",
            message: "Hangi İşlemi Yapmak İstiyorsun ?",
            choices: [
                {name: "Branch Oluşturmak"},
                {name: "JIRA da bir Issue Kapatmak ve Commit"},
                {name: "Sadece Commit"}
            ]
        },

        /* Branch Create */
        {
            type: "list",
            name: "branchCreate",
            message: "Oluşturmak İstediğiniz Branch Hangisi?",
            choices: [
                {name: "Develop"},
                {name: "Feature"},
                {name: "Release"},
                {name: "Hotfix"}
            ],
            when: function (answers) {
                return answers.proccess == 'Branch Oluşturmak';
            }
        },
        {
            type: "input",
            name: "featureName",
            message: "Feature İsmi Nedir ?",
            when: function (answers) {
                return answers.branchCreate == 'Feature';
            }
        },
        {
            type: "input",
            name: "releaseVersion",
            message: "Release Versionu Nedir ?",
            when: function (answers) {
                return answers.branchCreate == 'Release';
            }
        },
        {
            type: "input",
            name: "hotfixVersion",
            message: "Hotfix Versionu Nedir ?",
            when: function (answers) {
                return answers.branchCreate == 'Hotfix';
            }
        },

        /* Commit Action */
        {
            type: "list",
            name: "repo",
            message: "Hangi Branch\'e Commit Yapılacak?",
            choices: branchList,
            when: function (answers) {
                return answers.proccess !== 'Branch Oluşturmak';
            }
        },
        {
            type: 'input',
            name: 'comment',
            message: 'Commit Comment',
            when: function (answers) {
                return answers.proccess !== 'Branch Oluşturmak';
            }
        },

        /* JIRA Action */
        {
            type: "input",
            name: "username",
            message: "JIRA Username ?",
            when: function (answers) {
                return answers.proccess == 'JIRA da bir Issue Kapatmak ve Commit';
            }
        },
        {
            type: "password",
            name: "password",
            message: "JIRA Şifre ?",
            when: function (answers) {
                return answers.username;
            }
        },
        {
            type: "input",
            name: "issue",
            message: "Issue ID >> ",
            default: issueID,
            when: function (answers) {
                return answers.proccess !== 'Branch Oluşturmak';
            }
        },
        {
            type: "list",
            name: "action",
            message: "JIRA'da Yapacağınız İşlem Nedir ?",
            choices: [
                {name: "Comment"},
                {name: "Resolve"}
            ],
            when: function (answers) {
                return answers.password;
            }
        },
        {
            type: "list",
            name: "resolution",
            message: "Issue Resolution?",
            choices: [
                {name: "Fixed"},
                {name: "Wont Fix"},
                {name: "Duplicate"},
                {name: "Incomplete"},
                {name: "Cannot Reproduce"},
                {name: "Done"}
            ],
            when: function (answers) {
                return answers.action;
            }
        },

        /* Branch Task Action */
        {
            type: "confirm",
            name: "removeBranch",
            message: function(answers){
                return 'JIRA da (' + answers.issue  +') taskını (' + answers.action + ') yaptın. İlgili branchin (' + answers.repo  +') primary bir branch değil. Branch üzerindeki tüm işlemler tamam ise silelim mi?';
            },
            when: function (answers) {
                return  answers.proccess !== 'Branch Oluşturmak' && answers.repo !== 'master' || answers.repo !== 'develop' && answers.action == 'Resolve';
            }
        }
    ];

    inquirer.prompt(Questions).then(function (answers) {
        // Jira işlemlerinde seçili gelen parametreye göre ayrı ayrı ID tanımlaması yapılmak zorunda..
        switch (answers.action) {
            case 'Resolve':
                var actionID = 5;
                break;
            default:
        }

        var result = answers,
            jira = new JiraApi('https', 'jira.dogannet.tv', '', result.username, result.password, '2'),
            resolveResponse =  { "update": {"comment": [ { "add": {"body": "UXQuark v" + pkg + "\n\n" + result.comment + "\n\n Fixed: " + vFix} }]},"fields": {"resolution": {"name": result.resolution}}, "transition": { "id": actionID }};


        /*
         *  Develop Branch'i Oluşturuluyor...
         */
        if(result.branchCreate == 'Develop') {
            git.init(function() {
                require('child_process').exec('git branch develop');
            }).then(function() {
                console.log(chalk.red('Lütfen Bekleyin'));
            }).push(['-u', 'origin', 'develop']).then(function() {
                console.log(chalk.cyan('>>>> ' + chalk.yellow.bold('develop') + ' branch\'i oluşturuldu.'));
            }).checkout('develop').then(function() {
                console.log(chalk.cyan('>>>> Şuan çalışılan branch ' + chalk.yellow.bold('develop') + ' olarak değiştirildi.'));
            })
        }

        /*
         *  Feature Branch'i Oluşturuluyor...
         *  Eğer aynı isimde başka bir feature branch'i var ise Inquirer sorgusu ile silinip silinmeyeceği sorgulanacak.
         *  Feature Branch'leri default olarak 'develop' branchi üzerinden oluşturulacak.
         */
        if(result.branchCreate == 'Feature') {
            git.init().then(function() {
                require('child_process').exec('git flow feature start '+result.featureName, function(error) {
                    if(error) {
                        if(error.message.indexOf("already exists") > -1){
                            console.log('');
                            console.error(chalk.red.bold('>>> HATA : ' + chalk.cyan('Oluşturmak istediğiniz Branch isminden(' + chalk.yellow.bold('feature/'+result.featureName) + ') mevcut')));
                            console.log('');

                            var featureQuestion = [
                                {
                                    type: "list",
                                    name: "featureProcess",
                                    message: "Bu Feature Branch\'i ile ilgili ne yapmak istiyorsun ?",
                                    choices: [
                                        {name: "Checkout"},
                                        {name: "Finish"},
                                        {name: "New Feature"}
                                    ]
                                },
                                {
                                    type: "input",
                                    name: "newfeatureName",
                                    message: "Yeni Feature İsmini Yaz ?",
                                    when: function (answers) {
                                        return answers.featureProcess == 'New Feature';
                                    }
                                }
                            ];

                            inquirer.prompt(featureQuestion).then(function (answer) {
                                if(answer.featureProcess === 'Checkout'){
                                    git.init().then(function() {
                                        console.log(chalk.cyan('Lütfen bekleyin' + chalk.yellow.bold('feature/'+result.featureName) + ' checkout ediliyor.'));
                                    }).checkout('feature/'+result.featureName).then(function() {
                                        console.log(chalk.cyan('Şuan çalışılan branch ' + chalk.yellow.bold('feature/'+result.featureName) + ' olarak değiştirildi.'));
                                    })
                                }
                                if(answer.featureProcess === 'Finish'){
                                    require('child_process').exec('git flow feature finish -F '+result.featureName, function(error) {
                                        if(error) {
                                            console.log('');
                                            console.log(chalk.red('---- HATA BAŞLANGIÇ ----'));
                                            console.log(error);
                                            console.log(chalk.red('---- HATA SONU ----'));
                                        } else {
                                            console.log('');
                                            console.log(chalk.cyan('>>> '+ chalk.yellow.bold(result.featureName) + ' branch\'i ' + chalk.yellow.bold('develop') + ' branch\'i ile merge edildi ve silindi..'));
                                            git.init().then(function() {
                                                console.log(chalk.cyan('>>> ' + chalk.yellow.bold('develop') + ' branch\'i push ediliyor..'));
                                            }).push('origin', 'develop').then(function() {
                                                console.log(chalk.green('>>> Tamamlandı : ' + chalk.cyan(chalk.yellow.bold('develop') + ' branch\'i push edildi..')));
                                            })
                                        }
                                    });
                                }
                                if(answer.featureProcess === 'New Feature'){
                                    require('child_process').exec('git flow feature start '+answer.newfeatureName);
                                    console.log('');
                                    console.log(chalk.green.bold('>>> BAŞARILI : ' + chalk.white.bold(chalk.yellow.bold('feature/' + answer.newfeatureName) + ' feature branch\'i oluşturuldu.')));
                                }
                            });

                        } else {
                            console.log('');
                            console.error(chalk.red.bold('>>> HATA : ' + chalk.cyan('Şuan çalıştığınız branch ile '+ chalk.yellow.bold('develop/master') +' branchleri arasında çakışan dosyalarınız var ' + chalk.yellow.bold('Feature') + ' oluşturabilmek için '+ chalk.yellow.bold('develop/master') + ' branchinde olmanız gerekiyor.')));
                        }
                    } else {
                        console.log('');
                        console.log(chalk.green.bold('>>> BAŞARILI : ' + chalk.white.bold(chalk.yellow.bold('feature/' + result.featureName) + ' branch\'i ' + chalk.yellow.bold('develop') +' üzerinden oluşturuldu..')));
                    }
                });
            });
        }

        /*
         *  Release Branch'i Oluşturuluyor...
         *  Eğer aynı isimde başka bir release branch'i var ise Inquirer sorgusu ile silinip silinmeyeceği sorgulanacak.
         *  Release Branch'leri default olarak 'develop' branchi üzerinden oluşturulacak.
         */
        if(result.branchCreate == 'Release') {
            git.init().then(function() {
                require('child_process').exec('git flow release start '+result.releaseVersion, function(error) {
                    if(error) {
                        if(error.message.indexOf("Working tree contains unstaged changes") > -1){
                            console.log('');
                            console.error(chalk.red.bold('>>> UYARI : ' + chalk.cyan('Şuanda düzenlenmiş fakat commit/push edilmemiş dosyanız mevcut. Öncelikle o dosya(ları) commit/push edin...')));
                        }
                        if(error.message.indexOf("already exists") > -1){
                            console.log('');
                            console.error(chalk.red.bold('>>> UYARI : ' + chalk.cyan('Kullandığın Tag versionu daha önce tanımlanmış(' + chalk.yellow.bold(result.releaseVersion) + '). Aynı version numarası ile tekrardan oluşturulamazsın...')));
                            console.log('');
                            git.init().then(function() {
                                console.log(chalk.red('---- Kullanılan Tag Versionları ----'));
                            }).tags(function(err, tags) {
                                console.log("En Büyük Tanımlanmış Tag Versionu: %s", tags.latest);
                                console.log("Versionlar : %j", tags.all);
                                console.log('');

                                var releaseQuestion = [{
                                    type: "input",
                                    name: "newreleaseVersion",
                                    message: "Yeni Release Versionunu Yaz",
                                    validate: function(str){
                                        var TagList = tags.all;
                                        var TagFilter = _.contains(TagList, str);
                                        return TagFilter !== true;
                                    }
                                }];

                                inquirer.prompt(releaseQuestion).then(function (answer) {
                                    require('child_process').exec('git flow release start '+answer.newreleseVersion);
                                    console.log('');
                                    console.log(chalk.green.bold('>>> BAŞARILI : ' + chalk.white.bold(chalk.yellow.bold('release/' + answer.newreleaseVersion) + ' release branch\'i ' + chalk.yellow.bold('develop') +' üzerinden oluşturuldu..')));
                                });
                            });
                        }
                        if(error.message.indexOf("existing hotfix") > -1){
                            require('child_process').exec('git flow release list', function(error, stderr, stdout){
                                if(stderr[0] === '*'){
                                    var activeHotfix = stderr.replace(/\*|\s/g, "");
                                } else {
                                    var activeHotfix = stderr.replace(/\n|\s/g, "");
                                }
                                git.init().then(function(){
                                    console.log('');
                                    console.error(chalk.red.bold('>>> UYARI : ' + chalk.cyan('Release branchlerinden sadece 1 tane olabilir.. Şuan aktif olan bir hotfix branch\'iniz var..(' + chalk.yellow.bold(activeHotfix) + ')')));
                                    console.log('');
                                }).then(function(){
                                    var releaseQuestion = [
                                        {
                                            type: "list",
                                            name: "releaseProcess",
                                            message: "Bu Release Branch\'i ile ilgili ne yapmak istiyorsun ?",
                                            choices: [
                                                {name: "Checkout"},
                                                {name: "Finish"}
                                            ]
                                        },
                                        {
                                            type: "input",
                                            name: "tagMessage",
                                            message: "Tag Mesajı",
                                            when: function (answers) {
                                                return answers.releaseProcess == 'Finish';
                                            }
                                        }
                                    ];

                                    inquirer.prompt(releaseQuestion).then(function (answer) {
                                        if(answer.releaseProcess === 'Checkout'){
                                            git.init().then(function() {
                                                console.log(chalk.cyan('Lütfen bekleyin' + chalk.yellow.bold('release/'+activeHotfix) + ' checkout ediliyor.'));
                                            }).checkout('release/'+activeHotfix).then(function() {
                                                console.log(chalk.cyan('Şuan çalışılan branch ' + chalk.yellow.bold('release/'+activeHotfix) + ' olarak değiştirildi.'));
                                            })
                                        }
                                        if(answer.releaseProcess === 'Finish'){
                                            require('child_process').exec('git flow release finish '+activeHotfix+ ' -m ' + answer.tagMessage, function(error) {
                                                if(error) {
                                                    console.log('');
                                                    console.log(chalk.red('---- HATA BAŞLANGIÇ ----'));
                                                    console.log(error);
                                                    console.log(chalk.red('---- HATA SONU ----'));
                                                } else {
                                                    console.log('');
                                                    console.log(chalk.cyan('>>> '+ chalk.yellow.bold('release/' + activeHotfix) + ' branch\'i ' + chalk.yellow.bold('develop/master') + ' branch\'leri ile merge edildi ve silindi..'));
                                                    git.init().then(function() {
                                                        console.log(chalk.cyan('>>> ' + chalk.yellow.bold('develop/master') + ' branch\'leri push ediliyor..'));
                                                    }).push('origin', 'master').then(function() {
                                                        console.log('');
                                                        console.log(chalk.green('>>> Tamamlandı : ' + chalk.cyan(chalk.yellow.bold('master') + ' branch\'i push edildi..')));
                                                    }).push('origin', 'develop').then(function() {
                                                        console.log(chalk.green('>>> Tamamlandı : ' + chalk.cyan(chalk.yellow.bold('develop') + ' branch\'i push edildi..')));
                                                    }).pushTags('origin').then(function() {
                                                        console.log(chalk.green('>>> Tamamlandı : ' + chalk.cyan('Tags Eklendi')));
                                                        console.log('');
                                                        console.log(chalk.cyan('Şuan çalışılan branch ' + chalk.yellow.bold('develop') + ' olarak değiştirildi.'));
                                                    });
                                                }
                                            });
                                        }
                                    });
                                });
                            });
                        }
                    } else {
                        console.log('');
                        console.log(chalk.green.bold('>>> BAŞARILI : ' + chalk.white.bold(chalk.yellow.bold('release/' + result.releaseVersion) + ' release branch\'i ' + chalk.yellow.bold('develop') +' üzerinden oluşturuldu..')));
                    }
                });
            });
        }

        /*
         *  Maintenance Branch'i Oluşturuluyor...
         *  Eğer aynı isimde başka bir maintenance branch'i var ise Inquirer sorgusu ile silinip silinmeyeceği sorgulanacak.
         *  Hotfix Branch'leri default olarak 'master' branchi üzerinden oluşturulacak.
         */
        if(result.branchCreate == 'Hotfix') {
            git.init().then(function() {
                require('child_process').exec('git flow hotfix start '+result.hotfixVersion, function(error) {
                    if(error) {
                        if(error.message.indexOf("Working tree contains unstaged changes") > -1){
                            console.log('');
                            console.error(chalk.red.bold('>>> UYARI : ' + chalk.cyan('Şuanda düzenlenmiş fakat commit/push edilmemiş dosyanız mevcut. Öncelikle o dosya(ları) commit/push edin...')));
                        }
                        if(error.message.indexOf("already exists") > -1){
                            console.log('');
                            console.error(chalk.red.bold('>>> UYARI : ' + chalk.cyan('Kullandığın Tag versionu daha önce tanımlanmış(' + chalk.yellow.bold(result.hotfixVersion) + '). Aynı version numarası ile tekrardan oluşturulamazsın...')));
                            console.log('');
                            git.init().then(function() {
                                console.log(chalk.red('---- Kullanılan Tag Versionları ----'));
                            }).tags(function(err, tags) {
                                console.log("En Büyük Tanımlanmış Tag Versionu: %s", tags.latest);
                                console.log("Versionlar : %j", tags.all);
                                console.log('');

                                var hotfixQuestion = [{
                                    type: "input",
                                    name: "newhotfixVersion",
                                    message: "Yeni Hotfix Versionunu Yaz",
                                    validate: function(str){
                                        var TagList = tags.all;
                                        var TagFilter = _.contains(TagList, str);
                                        return TagFilter !== true;
                                    }
                                }];

                                inquirer.prompt(hotfixQuestion).then(function (answer) {
                                    require('child_process').exec('git flow hotfix start '+answer.newhotfixVersion);
                                    console.log('');
                                    console.log(chalk.green.bold('>>> BAŞARILI : ' + chalk.white.bold(chalk.yellow.bold('hotfix/' + answer.newhotfixVersion) + ' hotfix branch\'i ' + chalk.yellow.bold('master') +' üzerinden oluşturuldu..')));
                                });
                            });
                        }
                        if(error.message.indexOf("existing hotfix") > -1){
                            require('child_process').exec('git flow hotfix list', function(error, stderr, stdout){
                                if(stderr[0] === '*'){
                                    var activeHotfix = stderr.replace(/\*|\s/g, "");
                                } else {
                                    var activeHotfix = stderr.replace(/\n|\s/g, "");
                                }
                                git.init().then(function(){
                                    console.log('');
                                    console.error(chalk.red.bold('>>> UYARI : ' + chalk.cyan('Hotfix branchlerinden sadece 1 tane olabilir.. Şuan aktif olan bir hotfix branch\'iniz var..(' + chalk.yellow.bold(activeHotfix) + ')')));
                                    console.log('');
                                }).then(function(){
                                    var hotfixQuestion = [
                                        {
                                            type: "list",
                                            name: "hotfixProcess",
                                            message: "Bu Hotfix Branch\'i ile ilgili ne yapmak istiyorsun ?",
                                            choices: [
                                                {name: "Checkout"},
                                                {name: "Finish"}
                                            ]
                                        },
                                        {
                                            type: "input",
                                            name: "tagMessage",
                                            message: "Tag Mesajı",
                                            when: function (answers) {
                                                return answers.hotfixProcess == 'Finish';
                                            }
                                        }
                                    ];

                                    inquirer.prompt(hotfixQuestion).then(function (answer) {
                                        if(answer.hotfixProcess === 'Checkout'){
                                            git.init().then(function() {
                                                console.log(chalk.cyan('Lütfen bekleyin' + chalk.yellow.bold('hotfix/'+activeHotfix) + ' checkout ediliyor.'));
                                            }).checkout('hotfix/'+activeHotfix).then(function() {
                                                console.log(chalk.cyan('Şuan çalışılan branch ' + chalk.yellow.bold('hotfix/'+activeHotfix) + ' olarak değiştirildi.'));
                                            })
                                        }
                                        if(answer.hotfixProcess === 'Finish'){
                                            require('child_process').exec('git flow hotfix finish '+activeHotfix+ ' -m ' + answer.tagMessage, function(error) {
                                                if(error) {
                                                    console.log('');
                                                    console.log(chalk.red('---- HATA BAŞLANGIÇ ----'));
                                                    console.log(error);
                                                    console.log(chalk.red('---- HATA SONU ----'));
                                                } else {
                                                    console.log('');
                                                    console.log(chalk.cyan('>>> '+ chalk.yellow.bold('hotfix/' + activeHotfix) + ' branch\'i ' + chalk.yellow.bold('develop/master') + ' branch\'leri ile merge edildi ve silindi..'));
                                                    git.init().then(function() {
                                                        console.log(chalk.cyan('>>> ' + chalk.yellow.bold('develop/master') + ' branch\'leri push ediliyor..'));
                                                    }).push('origin', 'master').then(function() {
                                                        console.log('');
                                                        console.log(chalk.green('>>> Tamamlandı : ' + chalk.cyan(chalk.yellow.bold('master') + ' branch\'i push edildi..')));
                                                    }).push('origin', 'develop').then(function() {
                                                        console.log(chalk.green('>>> Tamamlandı : ' + chalk.cyan(chalk.yellow.bold('develop') + ' branch\'i push edildi..')));
                                                    }).pushTags('origin').then(function() {
                                                        console.log(chalk.green('>>> Tamamlandı : ' + chalk.cyan('Tags Eklendi')));
                                                        console.log('');
                                                        console.log(chalk.cyan('Şuan çalışılan branch ' + chalk.yellow.bold('develop') + ' olarak değiştirildi.'));
                                                    });
                                                }
                                            });
                                        }
                                    });
                                });
                            });
                        }
                    } else {
                        console.log('');
                        console.log(chalk.green.bold('>>> BAŞARILI : ' + chalk.white.bold(chalk.yellow.bold('hotfix/' + result.hotfixVersion) + ' hotfix branch\'i ' + chalk.yellow.bold('master') +' üzerinden oluşturuldu..')));
                    }
                });
            });
        }


        /* Function */
        var CommitAction = function(){
            if(result.proccess == 'JIRA da bir Issue Kapatmak ve Commit'){
                var jiraCommit = result.issue + ' #' + result.resolution + ' (' + result.comment + ')';
            }else if(result.proccess == 'Sadece Commit') {
                var jiraCommit = result.comment;
            }
            git.init().then(function() {
                console.log(chalk.red('Lütfen Bekleyin'));
                console.log(chalk.cyan('Pull İşlemi Başladı'));
            }).pull(function(err, update) {
                if(err == null){
                    if(update.files.length > 0){
                        console.log('');
                        console.log(chalk.white.bold('-------------' + chalk.cyan.bold('DOSYALAR') + '---------------'));
                        console.log(chalk.magenta.bold('Gelen Dosyalar ') + update.files);
                        console.log(chalk.white.bold('--------------------------------------'));
                        console.log('');
                    }
                    console.log(chalk.green('Pull İşlemi Tamamlandı'));
                    console.log('');
                    console.log(chalk.cyan('Dosyalar Indexleniyor'));
                } else {
                    console.log('');
                    console.log(chalk.red('HATA >>>>', err));
                    console.log('');
                    require('child_process').exec('process.exit()');
                    console.log(chalk.red.bold('DİKKAT : ' + chalk.white.bold('Yeni PULL içerisinde çakışan dosyalarınız var.. Öncelikle bu dosyaları GIT üzerinden merge ederek düzenlemesini yapınız')));
                    console.log(chalk.yellow.bold('git diff master origin/' + result.repo) + chalk.white.bold(' komutu ile çakışan dosyalarınızı görebilirsiniz..'));
                }
            }).add('./*').then(function() {
                console.log(chalk.green('Dosyalar Indexlendi'));
            }).commit(jiraCommit).then(function() {
                console.log('Dosyalar ' + chalk.cyan(result.repo) + ' reposuna ' + chalk.cyan(jiraCommit) + ' mesajı ile commit edildi');
            }).push('origin', result.repo).then(function() {
                console.log('Dosyalar ' + chalk.cyan(result.repo) + ' reposuna push edildi.');
            })
        };
        /* Function END  */

        /* nodeJS Process */
        if(result.proccess == 'Sadece Commit'){
            CommitAction();
        }
        if(result.proccess == 'JIRA da bir Issue Kapatmak ve Commit'){
            if(result.action == 'Comment'){
                jira.addComment(result.issue, result.comment, function(error, success){
                    if(success == 'Success') {
                        console.log(chalk.red.bold(answers.issue) + ' Numaralı Issue detayına ' + chalk.white.underline.bold(result.comment) + ' mesajınız eklenmiştir.');
                    } else {
                        console.log(chalk.white.bgRed.bold('addComment Bir HATA oluştu!'));
                    }
                });
            } else {
                jira.transitionIssue(result.issue, resolveResponse, function(error, response){
                    if(response == 'Success') {
                        console.log('Info >>>> Issue: ' + chalk.red.underline.bold(result.issue) + ' Comment: ' + chalk.white.underline.bold(result.comment) + ' Result: ' + chalk.white.underline.bold(result.action) + ' Resolution: ' + chalk.white.underline.bold(result.resolution));
                    } else {
                        console.log(chalk.white.bgRed.bold('transitionIssue Bir HATA oluştu!'));
                    }
                });
            }

            CommitAction();
        }
    });
});
